/******对话框效果**********
**可用于制作背包、菜单等效果*
**************************
接收当前房间指针
此房间内绘制原房间和对话框房间
实现弹出对话框效果
**************************/
#include "game.h"

//函数指针
//typedef void(*dialog_fun)(); 

//移动的房间
extern Room**rmpStr;
RmEve dialog_call;
dialog_fun dialog_draw;

//房间创建
void dialogCreate(void*rm)
{
 
}

//绘制房间
void dialogDraw(void*rm)
{
 Room*room = rm;
// Cam*cam = &(room->cam);
 
 //绘制原房间
 if((*rmpStr)->draw!=NULL)
 	(*rmpStr)->draw(*rmpStr);
 else
 	drawSenObjCam((*rmpStr)->sen,
  &(*rmpStr)->cam);
  
 //调用绘制
 (*dialog_draw)();
 
 //绘制当前房间
 drawSenObjCam(room->sen,
  &(room->cam));
}

//房间事件
void dialogEvent(int type,int p1,int p2)
{
 if(dialog_call!=NULL)
 {
  (*dialog_call)(type,p1,p2);
 }
}

//跳转房间
int gotoDialog(Room**str,
dialog_fun draw,RmEve call)
{
 //接收数据
 rmpStr = str;
 dialog_draw = draw;
 dialog_call = call;

 if(GM.rmDiao==NULL)
 {
  GM.rmDiao = 
  newRoom(SW,SH,100);
 }
 GM.rmDiao->create = dialogCreate;
 GM.rmDiao->draw = dialogDraw;
 GM.rmDiao->event = dialogEvent;
 gotoRoom(GM.rmDiao);
 return 0;
}

