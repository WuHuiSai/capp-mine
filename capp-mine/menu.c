#include "game.h"

Obj*menu_start;
Obj*menu_time;
Obj*menu_pvp;
Obj*menu_more;
Obj*menu_abou;

Obj*menu_snd;
Obj*menu_bgm;

//绘制按钮
void drawBtn(void*ob)
{
 Obj*obj = ob;
 Cam *cam=&(GM.rmMenu->cam);
 setSprActId(obj->spr,obj->act);
 setSprImgId(obj->spr,obj->img);
 
 drawSprV9(obj->spr,obj->x-cam->x,
 obj->y-cam->y,32,32,6,6);
 fnt_drawline(GM.fnt_24_cn,
 (char*)obj->data[0],
 obj->x-cam->x+22,
 obj->y-cam->y+13);
}


//房间创建
void menuCreate(void*rm)
{
 Spr* spr;
 Obj* obj;
 Room*room = rm;
 //菜单文字
 GM.fnt_24_cn = (_FNT*)
 getRes("fnt/font_24_cn.fnt",
  tFnt)->id;
 //提示文字
 GM.fnt_12_cn = (_FNT*)
 getRes("fnt/font_12_cn.fnt",
  tFnt)->id;
 //logo文字
 GM.fnt_64 = (_FNT*)
 getRes("fnt/font_64.fnt",tFnt)->id;

 spr = (Spr*)
 getRes("spr/btn_2.spr",tSpr)->id;
 obj = newObj(SW/2-97,SH-49*5,spr);
 obj->draw = drawBtn;
 obj->group = 1;
 obj->data[0] = (int32)"闯关";
 menu_start = obj;
 roomAddObj(room,obj);
 
 spr = (Spr*)
 getRes("spr/btn.spr",tSpr)->id;
 obj = newObj(SW/2,SH-49*5,spr);
 obj->draw = drawBtn;
 obj->group = 2;
 obj->data[0] = (int32)"六角";
 menu_time = obj;
 roomAddObj(room,obj);
 
 obj = newObj(SW/2,SH-49*4,spr);
 obj->draw = drawBtn;
 obj->group = 3;
 obj->data[0] = (int32)"对战";
 menu_pvp = obj;
 roomAddObj(room,obj);
 
 obj = newObj(SW/2,SH-49*3,spr);
 obj->draw = drawBtn;
 obj->group = 4;
 obj->data[0] = (int32)"帮助";
 menu_more = obj;
 roomAddObj(room,obj);
 
 obj = newObj(SW/2-97,SH-49*3,spr);
 obj->draw = drawBtn;
 obj->group = 5;
 obj->data[0] = (int32)"关于";
 menu_abou = obj;
 roomAddObj(room,obj);
 
 spr = (Spr*)
 getRes("spr/snd.spr",tSpr)->id;
 obj = newObj(SW-64,SH-50,spr);
 obj->act = !GM.snd;
 obj->group = 6;
 menu_snd = obj;
 roomAddObj(room,obj);
 
 spr = (Spr*)
 getRes("spr/bgm.spr",tSpr)->id;
 obj = newObj(SW-128,SH-50,spr);
 obj->act = !GM.bgm;
 obj->group = 7;
 menu_bgm = obj;
 roomAddObj(room,obj);
}

//绘制房间
void menuDraw(void*rm)
{
 Room*room = rm;
 Cam *cam=&(room->cam);
 
 drawRect(0-cam->x,0-cam->y,
 SW,SH,0xffffffff);
 
 char data[100];
 sprintf(data,"MINE");
 fnt_drawline(GM.fnt_64,data,
 90-cam->x,30-cam->y);
 sprintf(data,"SWEEPER");
 fnt_drawline(GM.fnt_64,data,
 15-cam->x,120-cam->y);
 drawSenObjCam(room->sen,cam);
}

//房间事件
void menuEvent(int type,int p1,int p2)
{
 Obj*obj;
 //切换页面
 if(MS_UP==type)
 {
  menu_start->act = 0;
  menu_time->act = 0;
  menu_pvp->act = 0;
  menu_more->act = 0;
  menu_abou->act = 0;

  obj = 
  pointSenObj(_room->sen,p1,p2);
  if(obj!=NULL)
  {
   //闯关
   if(obj->group==1)
   {
    createHome();
    //sound_bgm(bgm_main,1,1);
    gotoMove(&GM.rmMenu,
    &GM.rmHome,_RIGHT); 
   }
   else if(obj->group==2)
   {
    createHex();
    //sound_bgm(bgm_main,1,1);
    gotoMove(&GM.rmMenu,
    &GM.rmHex,_RIGHT); 
   }
   //帮助
   else if(obj->group==4)
   {
    gotoDialog(&GM.rmMenu,
    drawDlgHelp,helpDlgExe);
   }
   //关于
   else if(obj->group==5)
   {
    createAbou();
    gotoMove(&GM.rmMenu,
    &GM.rmAbou,_LEFT); 
   }
   else if(obj->group==6)
   {
    obj->act = !obj->act;
    GM.snd = !obj->act;
   }
   else if(obj->group==7)
   {
    obj->act = !obj->act;
    GM.bgm = !obj->act;
   }
   else
toast("\xbe\xb4\xc7\xeb\xc6\xda\xb4\xfd\xa3\xa1",0);

    sound_snd(snd_click,0,0);
  }
 }
 
 if(MS_DOWN==type
 ||MS_MOVE==type)
 {
  menu_start->act = 0;
  menu_time->act = 0;
  menu_pvp->act = 0;
  menu_more->act = 0;
  menu_abou->act = 0;

  obj = 
  pointSenObj(_room->sen,p1,p2);
  if(obj!=NULL)
  if(obj->group<=5)
   obj->act = 1;
 }
 
 //退出页面
 if(KY_UP==type)
 {
  switch(p1)
  {
   case _BACK:
    exitRoom(_room);
    exit();
   break;
  }
 }
}

//创建房间
int createMenu()
{
 if(GM.rmMenu==NULL)
 {
  GM.rmMenu = 
  newRoom(SW,SH,100);
 }
 GM.rmMenu->create = menuCreate;
 GM.rmMenu->draw = menuDraw;
 GM.rmMenu->event = menuEvent;
 return 0;
}
