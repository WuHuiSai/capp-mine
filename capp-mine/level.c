#include "game.h"

extern Obj*bomb_num;//炸弹数
//重组炸弹
extern void resetCube(ObjSen*link);
extern Obj*game_home;
extern Obj*game_next;
extern Obj*game_tim;
extern Obj*game_sta;

//当前关卡
int level;
int level_time;
//第一次运行
int is_run;
int is_start;

extern Room**rmpStr;


//跳转下个关卡
void showNextDlg(Room**rmp,int start)
{
 is_start = start;
 rmpStr = rmp;
 
 if(level<MAX_LEVEL)
 {
  if(is_start)
  {
   level++;
   one_bomb = TRUE;
   game_sta->img = 0;
   resetCube((*rmpStr)->sen);
   bomb_num->data[1] = 
   getCubeBomb((*rmpStr)->sen);
  }
  if(*rmpStr==GM.rmHome)
  gotoDialog(&GM.rmHome,
    drawNextDlg,nextDlgEve);
  else
  gotoDialog(&GM.rmHex,
    drawNextDlg,nextDlgEve);
 }
 //游戏通关
 else
 {
  gotoDialog(rmpStr,
   drawWinDlg,winDlgEve);
 }
}

//绘制关卡
void drawNextDlg()
{
 Obj*obj;
 char data[200];
 Room*room = _room;
 Cam*cam = &(room->cam);
 drawRect(0-cam->x,0-cam->y,
  SW,SH,0x50000000);
 
 //进入关卡显示
 if(is_start)
 {
  drawRect(50-cam->x,SH/3-20-cam->y,
   SW-100,100,0xffffffff);
  
  drawRect(50-cam->x,SH/3-20-cam->y,
   SW-100,100,0x80ff0000);
  
  sprintf(data,"          关卡：%d\n\n          炸弹：%d",
   level,bomb_num->data[1]);
  fnt_drawline(GM.fnt_20_cn,
   data,20,SH/3);
  
  level_time--;
  if(level_time<=0)
  {
   //重置计时器
   game_tim->data[1] = 0;
   game_tim->data[2] = getuptime();
   
   level_time = TIM_LEVEL;
   _room = *rmpStr;
  }
 }
 //关卡结束显示
 else
 {
  drawRect(50-cam->x,SH/3-20-cam->y,
   SW-100,150,0xffffffff);
  
  drawRect(50-cam->x,SH/3-20-cam->y,
   SW-100,150,0x80ff0000);
  sprintf(data,
   "    关卡:%d，过关！\n\n    耗时:%d秒",
   level,game_tim->data[1]);
  fnt_drawline(GM.fnt_20_cn,
   data,50-cam->x,SH/3-cam->y);
  
  obj = game_home;
  obj->view = TRUE;
  obj->x = 50+32-cam->x;
  obj->y = SH/3+80-cam->x;
  drawObjPos(obj,obj->x-cam->x,
   obj->y-cam->y);
  
  obj = game_next;
  obj->view = TRUE;
  obj->x = SW-80-30-cam->x;
  obj->y = SH/3+80-cam->x;
  drawObjPos(obj,obj->x-cam->x,
   obj->y-cam->y);
 }
}

//下一关
void nextDlgEve(int type,int p1,int p2)
{
 if(is_start)
 	return;
 
 //抬起按键
 if(MS_UP==type)
 {
  game_home->act = 0;
  game_next->act = 0;
  //真正下一关
  if(_posCollObj(p1,p2,game_next))
  {
   game_home->view = FALSE;
   game_next->view = FALSE;
   
   _room = *rmpStr;
   showNextDlg(rmpStr,TRUE);
  }
  else
  	//返回首页
  if(_posCollObj(p1,p2,game_home))
  {
   game_home->view = FALSE;
   game_next->view = FALSE;
   
   _room = *rmpStr;
   if(_room->event!=NULL)
    	_room->event(KY_UP,_BACK,0);
  }
 }
 
 //按键效果
 if(MS_DOWN==type
   ||MS_MOVE==type)
 {
  game_home->act = 0;
  game_next->act = 0;
  
  if(_posCollObj(p1,p2,game_home))
  {
   game_home->act = 1;
  }
  else
  	if(_posCollObj(p1,p2,game_next))
  {
   game_next->act = 1;
  }
 }
}
