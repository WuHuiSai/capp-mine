/******房间切换效果*******
接收前后两个房间指针
此房间内绘制需切换的两个房间
移动摄像机实现房间移动效果
************************/
#include "game.h"

//移动的房间
Room**rmpStr;
Room**rmpEnd;
int move_type;

//房间创建
void moveCreate(void*rm)
{
 //改变视野
 if(move_type==_RIGHT)
 {
  (*rmpStr)->cam.x = 0;
  (*rmpEnd)->cam.x = -SW;
 }
 else
 {
  (*rmpStr)->cam.x = 0;
  (*rmpEnd)->cam.x = SW;
 }
}

//绘制房间
void moveDraw(void*rm)
{
 Room*room = rm;
 drawSenObjCam(room->sen,
  &(room->cam));
 
 //绘制两个过渡房间
 if((*rmpStr)->draw!=NULL)
 {
  _room = *rmpStr;
 	(*rmpStr)->draw(*rmpStr);
 }
 else
 	drawSenObjCam((*rmpStr)->sen,
  &(*rmpStr)->cam);
 
 if((*rmpEnd)->draw!=NULL)
 {
  _room = *rmpEnd;
 	(*rmpEnd)->draw(*rmpEnd);
 }
 else
 	drawSenObjCam((*rmpEnd)->sen,
  &(*rmpEnd)->cam);
 _room = GM.rmMove;
 
 if(move_type==_RIGHT)
 {
  //移动两个房间
  (*rmpStr)->cam.x+=60;
  (*rmpEnd)->cam.x+=40;
  //移动到位
  if((*rmpEnd)->cam.x>=0)
  {
   //跳转页面
   (*rmpStr)->cam.x = 0;
   (*rmpEnd)->cam.x = 0;
   
   exitRoom(*rmpStr);
   _room = *rmpEnd;
   
   *rmpStr = NULL;
   return;
  }
 }
 else
 {
  //移动两个房间
  (*rmpStr)->cam.x-=60;
  (*rmpEnd)->cam.x-=40;
  //移动到位
  if((*rmpEnd)->cam.x<=0)
  {
   //跳转页面
   (*rmpStr)->cam.x = 0;
   (*rmpEnd)->cam.x = 0;
   
   exitRoom(*rmpStr);
   _room = *rmpEnd;
   
   *rmpStr = NULL;
   return;
  }
 }
}
//房间事件
void moveEvent(int type,int p1,int p2)
{
 
}

//跳转房间 左右上下
int gotoMove(Room**str,Room**end,int type)
{
 //接收数据
 rmpStr = str;
 rmpEnd = end;
 move_type = type;
 
 //创建房间
 if((*rmpEnd)->create!=NULL)
 	(*rmpEnd)->create(*rmpEnd);
 
 if(GM.rmMove==NULL)
 {
  GM.rmMove = 
  newRoom(SW,SH,100);
 }
 GM.rmMove->create = moveCreate;
 GM.rmMove->draw = moveDraw;
 GM.rmMove->event = moveEvent;
 gotoRoom(GM.rmMove);
 return 0;
}
