#include "game.h"

int maxInt(int a,int b)
{
 return a>b?a:b;
}

int minInt(int a,int b)
{
 return a<b?a:b;
}

//定时器回调
//运行并绘制当前房间
void gameRun(int32 data)
{
 cls(0xff,0xff,0xff);
// runCurRoom();
 drawCurRoom();
 ref(0,0,SW,SH);
}

//入口函数
int init()
{
 cls(255,255,255);
 setscrsize(320,320*SH/SW);
 switchPath("c");
 
 //初始化定时器
 GM.timer=
 getRes("GM.timer",tTim)->id;
 timerstart(GM.timer,30,0,
 gameRun,TRUE);
 
 //游戏模式
 GM.mode = 0;
 GM.snd = TRUE;
 GM.bgm = FALSE;
 soundinit_all();
 
 //初始化房间
 createMenu();
 gotoRoom(GM.rmMenu);
 
 //刷新屏幕
 ref(0,0,SW,SH);
 return 0;
}

//event函数
int event(int type,int p1,int p2)
{
 if(_room->event!=NULL)
 	_room->event(type,p1,p2);
 return 0;
}

//应用暂停
int pause()
{
 return 0;
}

//应用恢复
int resume()
{
 return 0;
}

//退出函数
int exitApp()
{
 soundclose_all();
 freeRes();
 return 0;
}