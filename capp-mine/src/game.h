#ifndef GAME_H
#define GAME_H
//声明游戏内结构，函数等
#include "def.h"

//升级期间
#define TIM_LEVEL 50
//最大关卡
#define MAX_LEVEL 10

//关卡页面
extern int level;//当前关卡
extern int level_time;//等待时间
extern int is_run;//第一次运行
extern void showNextDlg(Room**rmp,
int start);//跳转关卡
extern void drawNextDlg();//绘制卡片
extern void nextDlgEve(int type,int p1,int p2);

//胜利页面
#define TIM_WIN 100
extern int win_time;
extern void showWinDlg();
extern void drawWinDlg();
extern void winDlgEve(int type,int p1,int p2);

//失败页面
extern void drawFailDlg();
extern void failDlgEve(int type,int p1,int p2);

//图片宽高
extern int IMG_W;
//横向数目
extern int MAP_N;//SW/IMG_W-1;
//纵向数目
extern int MAP_M;//((SH-80)/IMG_W-1)
#define MAP_W (MAP_N*IMG_W)

//递归深度
extern int DEP;
//第一次点击
extern int one_bomb;

extern int isWin(ObjSen*link,int is);
extern void resetCube(ObjSen*link);
extern int getCubeBomb(ObjSen*link);
extern void touchCube(Obj*obj);

#define snd_click "assets://snd/click.mp3"
#define snd_dead "assets://snd/dead.mp3"
#define snd_pop "assets://snd/pop.wav"

#define bgm_main "assets://bgm/main.mp3"

extern void sound_snd(char*name,int stype,int loop);
extern void sound_bgm(char*name,int stype,int loop);
extern void soundinit_all();
extern void soundclose_all();

//游戏变量
struct _game
{
 int32 timer;//全局定时器
 _FNT*fnt_12_cn;//全局字体
 _FNT*fnt_20;//全局字体
 _FNT*fnt_20_cn;//全局字体
 _FNT*fnt_24;//全局字体
 _FNT*fnt_24_cn;//全局字体
 _FNT*fnt_64;//首页标题
 
 Room*rmHome;//主页
 Room*rmMenu;//菜单
 Room*rmAbou;//关于
 Room*rmHex;//编辑器
 
 Room*rmDiao;//对话框
 Room*rmMove;//过渡房间
 
 int mode;//游戏模式
 // 0闯关 1计时 2地图编辑 3地图运行
 int snd; //TRUE FALSE
 int bgm; //TRUE FALSE
}GM;

//rmHome 主页
extern void homeCreate(void*rm);
extern void homeDraw(void*rm);
extern void homeEvent(int type,int p1,int p2);
extern int createHome();

//六边形
extern void hexCreate(void*rm);
extern void hexDraw(void*rm);
extern void hexEvent(int type,int p1,int p2);
extern int createHex();

//菜单页
extern void menuCreate(void*rm);
extern void menuDraw(void*rm);
extern void menuEvent(int type,int p1,int p2);
extern int createMenu();

//帮助页
extern void abouCreate(void*rm);
extern void abouDraw(void*rm);
extern void abouEvent(int type,int p1,int p2);
extern int createAbou();

//对话框
typedef void(*dialog_fun)();
extern int gotoDialog(Room**str,
 dialog_fun draw,RmEve call);

//帮助对话框
extern void helpDlgExe(int type,int p1,int p2);
void drawDlgHelp();

//过渡房间
extern int gotoMove(Room**str,Room**end,int type);

extern int maxInt(int a,int b);
extern int minInt(int a,int b);

#endif