#include "game.h"

//退出帮助
void helpDlgExe(int type,int p1,int p2)
{
 if(MS_UP==type)
 {
  _room = GM.rmMenu;
 }
}



//绘制帮助
void drawDlgHelp()
{
 Room*room = _room;
 Cam*cam = &(room->cam);
 drawRect(0-cam->x,0-cam->y,
  SW,SH,0xf0000000);
 
 fnt_drawline(GM.fnt_12_cn,
  "总炸弹数: 显示本局总炸弹数，找到它们！",
  20-cam->x,SH-220-cam->y);
 
 fnt_drawline(GM.fnt_12_cn,
  "游戏时间: 显示本局游戏用时",
  20-cam->x,SH-180-cam->y);
 
 fnt_drawline(GM.fnt_12_cn,
  "切    换: 点击切换按钮，标识炸弹",
  20-cam->x,SH-140-cam->y);
 
 fnt_drawline(GM.fnt_12_cn,
  "标    记: 长按方块标记为地雷",
  20-cam->x,SH-100-cam->y);
 
 drawRect(20-cam->x,
  SH-50-cam->y,150,30,0x80ff0000);
 fnt_drawline(GM.fnt_12_cn,
  "点击屏幕，返回游戏！",20+5-cam->x,
  SH-50+10-cam->y);
}

