#include "game.h"

extern int*bomb_arr;
extern int depth;
extern Obj*game_sta;

//获取周围炸弹数
int getHexBombNum(Obj*obj)
{
 Obj*next;
 int num = 0;
 int x = obj->x-IMG_W/2;
 int y = obj->y-IMG_W/2-IMG_W/8;
 //六方向判断
 for(int i = 0;i<3;i++)
 	for(int j = 0;j<2;j++)
 {
  next = pointSenObj(_room->sen,
   x+i*IMG_W/2,
   y+j*(IMG_W+IMG_W/4));
  if(next!=NULL)
  {
   //查到炸弹
   if(next->data[0]==TRUE)
   	num++;
  }
 }
 return num;
}

//判断点击
void runHexCube(Obj*obj)
{
 Obj*next;
 int num = 0;
 int x = obj->x-IMG_W/2;
 int y = obj->y-IMG_W/2;
 
 //自身是炸弹
 if(obj->data[0]==TRUE)
 	return;
 
 //周围炸弹数
 num = getHexBombNum(obj);
 obj->data[1] = 15-num;
 
 //六方向递归
 for(int i = 0;i<3;i++)
 	for(int j = 0;j<2;j++)
 {
  next = pointSenObj(_room->sen,
   x+i*IMG_W/4*3,
   y+j*IMG_W/4*3);
  if(next!=NULL)
  {
   //数字不为0停止 
   if(num<=0)
   	if(next->data[1]==0)
   {
    //递归深度
    //if(depth<DEP)
    {
     depth++;
     runHexCube(next);
    }
   }
   
   //未插旗
   //非炸弹显示
   if(next->data[1]==0)
   	if(next->data[0]==FALSE)
   {
    num = getHexBombNum(next);
    next->data[1] = 15-num;
   }
  }
 }
}

//点击棋盘
void touchHexCube(Obj*obj)
{
 int sta = game_sta->img;
  
 //排雷模式
 if(sta==0)
 {
  //点击到炸弹
  if(obj->data[1]!=1&&
    obj->data[1]!=2)
  	if(obj->data[0]==TRUE)
  {
   //第一次点击
   if(one_bomb!=TRUE)
   {
    obj->data[1] = 4;
    //游戏失败
    isWin(_room->sen,TRUE);
    //重来页面
    {
     sound_snd(snd_dead,0,0);
     gotoDialog(&GM.rmHex,
      drawFailDlg,failDlgEve);
    }
    return;
   }
   else
   {
    obj->data[1] = 1;
    //one_bomb = FALSE;
   }
  }
  
  //未点击炸弹
  if(obj->data[1]==0)
  {
   depth = 0;
   runHexCube(obj);
  }
  
  //第一次点击使用
  one_bomb = FALSE;
 }
 //插旗模式
 else
 {
  if(obj->data[1]<=2)
  {
   obj->data[1]++;
   if(obj->data[1]>2)
   	obj->data[1] = 0;
  }
 }
 
 //游戏胜利
 if(isWin(_room->sen,FALSE))
 {
  isWin(_room->sen,TRUE);
  //进入下一关
  showNextDlg(&_room,FALSE);
 }
}


