#include "game.h"

extern int*bomb_arr;
extern int depth;
extern Obj*game_sta;

//获取总炸弹数
int getCubeBomb(ObjSen*link)
{
 int num = 0;
 ObjSen*temp = link;
 while(TRUE)
 {
  if(temp==NULL)
   return num;
  if(temp->obj!=NULL)
  	if(temp->obj->group==1)
  {
   if(temp->obj->data[0]==TRUE)
   	num++;
  }
  temp = temp->next;
 }
}


//重组炸弹
void resetCube(ObjSen*link)
{
 //cnt当前位置，index炸弹位置
 int cnt = 0,index = 0;
 
 //随机种子
 sand(getuptime());
 
 //初始化数组
 for(int i = 0;i<MAP_N*MAP_M;i++)
 {
  bomb_arr[i] = i;
 }
 
 //打乱数组
 int tmp,ran;
 for(int i = 0;i<MAP_N*MAP_M;i++)
 {
  ran = rand()%(MAP_N*MAP_M);
  tmp = bomb_arr[ran];
  bomb_arr[ran] = bomb_arr[i];
  bomb_arr[i] = tmp;
 }
 
 //数组排序
 for(int j = 0;j<level*MAP_M/2+5;j++)
 	for(int i = 0;i<level*MAP_M/2+5;i++)
 {
  if(bomb_arr[i]>bomb_arr[j])
  {
   tmp = bomb_arr[j];
   bomb_arr[j] = bomb_arr[i];
   bomb_arr[i] = tmp;
  }
 }
 
 //遍历链表
 ObjSen*temp = link;
 while(TRUE)
 {
  if(temp==NULL)
  {
   return;
  }
  if(temp->obj!=NULL)
  	if(temp->obj->group==1)
  {
   temp->obj->data[1] = 0;
   temp->obj->data[0] = FALSE;
   if(cnt==bomb_arr[index])
   	if(index<level*MAP_M/2+5)
   {
    index++;
    temp->obj->data[0] = TRUE;
   }
   cnt++;
  }
  temp = temp->next;
 }
}

//获取周围炸弹数
int getBombNum(Obj*obj)
{
 Obj*next;
 int num = 0;
 int x = obj->x-IMG_W/2;
 int y = obj->y-IMG_W/2;
 //八方向判断
 for(int i = 0;i<3;i++)
 	for(int j = 0;j<3;j++)
 {
  next = pointSenObj(_room->sen,
   x+i*IMG_W,y+j*IMG_W);
  if(next!=NULL)
  {
   //查到炸弹
   if(next->data[0]==TRUE)
   	num++;
  }
 }
 return num;
}


//判断点击
void runCube(Obj*obj)
{
 Obj*next;
 int num = 0;
 int x = obj->x-IMG_W/2;
 int y = obj->y-IMG_W/2;
 
 //自身是炸弹
 if(obj->data[0]==TRUE)
 	return;
 
 //周围炸弹数
 num = getBombNum(obj);
 obj->data[1] = 15-num;
 
 //四方向递归
 for(int i = 0;i<3;i++)
 	for(int j = 0;j<3;j++)
 	if((i==0&&j==1)
   ||(i==1&&j==0)
   ||(i==1&&j==2)
   ||(i==2&&j==1))
 {
  next = pointSenObj(_room->sen,
   x+i*IMG_W,y+j*IMG_W);
  if(next!=NULL)
  {
   //数字不为0停止 
   if(num<=0)
   	if(next->data[1]==0)
   {
    //递归深度
    //if(depth<DEP)
    {
     depth++;
     runCube(next);
    }
   }
   
   //未插旗
   //非炸弹显示
   if(next->data[1]==0)
   	if(next->data[0]==FALSE)
   {
    num = getBombNum(next);
    next->data[1] = 15-num;
   }
  }
 }
}

//点击棋盘
void touchCube(Obj*obj)
{
 int sta = game_sta->img;
  
 //排雷模式
 if(sta==0)
 {
  //点击到炸弹
  if(obj->data[1]!=1&&
    obj->data[1]!=2)
  	if(obj->data[0]==TRUE)
  {
   //第一次点击
   if(one_bomb!=TRUE)
   {
    obj->data[1] = 4;
    //游戏失败
    isWin(_room->sen,TRUE);
    //重来页面
    {
     sound_snd(snd_dead,0,0);
     gotoDialog(&GM.rmHome,
      drawFailDlg,failDlgEve);
    }
    return;
   }
   else
   {
    obj->data[1] = 1;
    //one_bomb = FALSE;
   }
  }
  
  //未点击炸弹
  if(obj->data[1]==0)
  {
   depth = 0;
   runCube(obj);
  }
  
  //第一次点击使用
  one_bomb = FALSE;
 }
 //插旗模式
 else
 {
  if(obj->data[1]<=2)
  {
   obj->data[1]++;
   if(obj->data[1]>2)
   	obj->data[1] = 0;
  }
 }
 
 //游戏胜利
 if(isWin(_room->sen,FALSE))
 {
  isWin(_room->sen,TRUE);
  //进入下一关
  showNextDlg(&_room,FALSE);
 }
}

