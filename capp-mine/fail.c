#include "game.h"

//重组炸弹
extern void resetCube(ObjSen*link);
extern Obj*game_home;
extern Obj*game_reload;
extern Room**rmpStr;

void drawFailDlg()
{
 Obj*obj;
 char data[200];
 Room*room = _room;
 Cam*cam = &(room->cam);
 drawRect(0-cam->x,0-cam->y,
  SW,SH,0x50000000);
 
 drawRect(50-cam->x,SH/3-20-cam->y,
  SW-100,150,0xffffffff);
 drawRect(50-cam->x,SH/3-20-cam->y,
  SW-100,150,0x80ff0000);
 sprintf(data,
  "   游戏结束，关卡:%d",level);
 fnt_drawline(GM.fnt_20_cn,
  data,50-cam->x,SH/3-cam->y);
 
 obj = game_home;
 obj->view = TRUE;
 obj->x = 50+32-cam->x;
 obj->y = SH/3+80-cam->x;
 drawObjPos(obj,obj->x-cam->x,
  obj->y-cam->y);
 
 obj = game_reload;
 obj->view = TRUE;
 obj->x = SW-80-30-cam->x;
 obj->y = SH/3+80-cam->x;
 drawObjPos(obj,obj->x-cam->x,
  obj->y-cam->y);
}

void failDlgEve(int type,int p1,int p2)
{
 //抬起按键
 if(MS_UP==type)
 {
  game_home->act = 0;
  game_reload->act = 0;
  //重新游戏
  if(_posCollObj(p1,p2,game_reload))
  {
   game_home->view = FALSE;
   game_reload->view = FALSE;
   
   level = 0;
   _room = *rmpStr;
   showNextDlg(rmpStr,TRUE);
  }
  else
  //返回首页
  if(_posCollObj(p1,p2,game_home))
  {
   game_home->view = FALSE;
   game_reload->view = FALSE;
   
   _room = *rmpStr;
   if(_room->event!=NULL)
    	_room->event(KY_UP,_BACK,0);
  }
 }
 
 //按键效果
 if(MS_DOWN==type
   ||MS_MOVE==type)
 {
  game_home->act = 0;
  game_reload->act = 0;
  
  if(_posCollObj(p1,p2,game_home))
  {
   game_home->act = 1;
  }
  else
  if(_posCollObj(p1,p2,game_reload))
  {
   game_reload->act = 1;
  }
 }
}

