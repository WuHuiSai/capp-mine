#include "game.h"

//房间创建
void abouCreate(void*rm)
{
 //载入资源
 GM.fnt_24_cn = (_FNT*)
 getRes("fnt/font_24_cn.fnt",
 tFnt)->id;
}

//绘制房间
void abouDraw(void*rm)
{
 Room*room = rm;
 Cam *cam=&(room->cam);
 drawSenObjCam(room->sen,cam);
 fnt_drawline(GM.fnt_24_cn,
 "作者: 蟋蟀蝈蝈蛐蛐",50-cam->x,
 40-cam->y);
 fnt_drawline(GM.fnt_24_cn,
 "QQ: 1126390395",50-cam->x,
 80-cam->y);
 fnt_drawline(GM.fnt_24_cn,
 "QQ交流群: 370468001",50-cam->x,
 120-cam->y);
}

//房间事件
void abouEvent(int type,int p1,int p2)
{
 //切换页面
 if(KY_DOWN==type)
 {
  createMenu();
  gotoMove(&GM.rmAbou,
  &GM.rmMenu,_RIGHT);
 }
}

//创建房间
int createAbou()
{
 if(GM.rmAbou==NULL)
 {
  GM.rmAbou = 
  newRoom(SW,SH,100);
 }
 GM.rmAbou->create = abouCreate;
 GM.rmAbou->draw = abouDraw;
 GM.rmAbou->event = abouEvent;
 return 0;
}
