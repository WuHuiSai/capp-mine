#include "game.h"

//炸弹数，游戏时间，鼠标，插旗
Obj*bomb_num;//炸弹数
Obj*game_tim;//游戏时间
Obj*game_pos;
Obj*game_sta;//插旗模式

//返回主页、重载、下一关
Obj*game_home;
Obj*game_reload;
Obj*game_next;

//长按
int long_time;
Obj*long_obj;

int IMG_W;
int MAP_N;
int MAP_M;

//递归深度
int DEP = 8000;
int depth = 0;
int one_bomb;

//存放炸弹位置
int*bomb_arr;

//音乐播放 stype 音乐设备类型 loop 0-单次 1-循环
void sound_snd(char *name,int stype,int loop)
{
 if(GM.snd)
 {
  soundloadfile(stype,name);
  soundplay(stype,0,loop);
  setvolume(5);
 }
}

void sound_bgm(char *name,int stype,int loop)
{
 if(GM.bgm)
 {
  soundloadfile(stype,name);
  soundplay(stype,0,loop);
  setvolume(5);
 }
}

//开启游戏所需的所有音频设备
void soundinit_all()
{
 for(int i=0;i<4;i++)
  soundinit(i);
}

//关闭所有音频设备
void soundclose_all()
{
 for(int i=0;i<4;i++)
  soundclose(i);
}

//绘制炸弹
//data[0]存数据 TRUE/FALSE
//data[1]存显示 0-16
void drawCube(void*ob)
{
 Obj*obj = ob;
// Cam*cam = &(GM.rmHome->cam);
 Cam*cam = &(_room->cam);
 obj->img = obj->data[1];
 drawObjPos(obj,
  obj->x-cam->x,obj->y-cam->y);
}

//绘制数码
//data[0]
//data[1]存数字
void drawNumSpr(void*ob)
{
 char data[200];
 Obj*obj = ob;
// Cam*cam = &(GM.rmHome->cam);
 Cam*cam = &(_room->cam);
 drawSpr(obj->spr,obj->x-cam->x,
  obj->y-cam->y);
 //绘制数字
 sprintf(data,"x%d",obj->data[1]);
 fnt_drawline(GM.fnt_24,data,
  obj->x+30-cam->x,obj->y-cam->y);
}

//房间创建
void homeCreate(void*rm)
{
 Obj*obj;
 Spr*spr;
 Room*room = rm;
 
 //页面文字
 GM.fnt_24_cn = (_FNT*)
 getRes("fnt/font_24_cn.fnt",
  tFnt)->id;
 
 GM.fnt_20_cn = (_FNT*)
 getRes("fnt/font_20_cn.fnt",
  tFnt)->id;
 
 GM.fnt_24 = (_FNT*)
 getRes("fnt/font_24.fnt",
  tFnt)->id;
 
 //炸弹数
 spr = (Spr*)
 getRes("spr/flag.spr",tSpr)->id;
 obj = newObj(20,20,spr);
 obj->img = 1;
 obj->draw = drawNumSpr;
 obj->data[0] = 0;
 obj->data[1] = 0;
 bomb_num = obj;
 roomAddObj(room,obj);
 
 //游戏时间
 spr = (Spr*)
 getRes("spr/timer.spr",tSpr)->id;
 obj = newObj(100,20,spr);
 obj->img = 1;
 obj->draw = drawNumSpr;
 obj->data[0] = 0;
 obj->data[1] = 0;
 obj->data[2] = getuptime();
 game_tim = obj;
 roomAddObj(room,obj);
  
 //横纵数目
 IMG_W = 24;
 MAP_N = SW/IMG_W-1;
 MAP_M = (SH-80)/IMG_W-1;

 //循环添加炸弹
 spr = (Spr*)
 getRes("spr/block.spr",tSpr)->id;
 for(int i = 0;i<MAP_N;i++)
 	for(int j = 0;j<MAP_M;j++)
 {
  obj = newObj((SW-MAP_W)/2+i*IMG_W,
   (SW-MAP_W)/2+60+j*IMG_W,spr);
  obj->data[0] = FALSE;
  obj->data[1] = 0;
  obj->group = 1;
  obj->draw = drawCube;
  roomAddObj(room,obj);
 }
 
 //插旗模式
 obj = newObj(SW-IMG_W-15,20,spr);
 obj->img = 0;
 game_sta = obj;
 roomAddObj(room,obj);
 
 //游戏鼠标
 spr = (Spr*)
 getRes("spr/focus.spr",tSpr)->id;
 obj = newObj(SW/2,SH/2,spr);
 game_pos = obj;
 roomAddObj(room,obj);
 
 //返回主页、重载、下一关
 spr = (Spr*)
 getRes("spr/home.spr",tSpr)->id;
 obj = newObj(0,0,spr);
 obj->view = FALSE;
 game_home = obj;
 roomAddObj(room,obj);
 spr = (Spr*)
 getRes("spr/reload.spr",tSpr)->id;
 obj = newObj(0,0,spr);
 obj->view = FALSE;
 game_reload = obj;
 roomAddObj(room,obj);
 spr = (Spr*)
 getRes("spr/next.spr",tSpr)->id;
 obj = newObj(0,0,spr);
 obj->view = FALSE;
 game_next = obj;
 roomAddObj(room,obj);
 
 //关卡
 one_bomb = TRUE;//第一次点击未使用
 level = 0;
 is_run = FALSE;
 level_time = TIM_LEVEL;

 //存放炸弹位置
 bomb_arr = 
 malloc(sizeof(int)*MAP_N*MAP_M);
 game_sta->img = 0;
 resetCube(room->sen);
 bomb_num->data[1] = 
 getCubeBomb(GM.rmHome->sen);
}


//绘制房间
void homeDraw(void*rm)
{
 Room*room = rm;
 Cam*cam = &(room->cam);
 
 //第一局
 if(_room==room)
 if(cam->x==0)
 	if(is_run==FALSE)
 {
  is_run = TRUE;
  showNextDlg(&GM.rmHome,TRUE);
 }
 
 //时间计时
 if(_room==room)
 {
  game_tim->data[1] = (getuptime()-
   game_tim->data[2])/1000;
 }
 //暂停计时
 else
 {
  game_tim->data[2] = getuptime()
  -game_tim->data[2]+getuptime();
 }

 //绘制房间
 drawSenObjCam(room->sen,cam);
}


//检测是否胜利，是否揭开
//只剩下炸弹
int isWin(ObjSen*link,int is)
{
 int result = TRUE;
 ObjSen*temp = link;
 
 while(TRUE)
 {
  if(temp==NULL)
   	return result;
   
  if(temp->obj!=NULL)
  	if(temp->obj->group==1)
  {
   //本身是炸弹
   if(temp->obj->data[0]==TRUE)
   {
    if(is)
    {
     if(temp->obj->data[1]==1)
     	temp->obj->data[1] = 3;
     else
     	temp->obj->data[1] = 4;
    } 
   }
   //本身不是炸弹
   else
   {
    //非炸弹插了旗，一定输
    if(temp->obj->data[1]==1)
    	result = FALSE;
    //非炸弹没揭开
    else if(temp->obj->data[1]==0
    ||temp->obj->data[1]==2)
     result = FALSE;
   }
  }
  temp = temp->next;
 }
}


//房间事件
void homeEvent(int type,int p1,int p2)
{
 Obj*obj;
 
 game_pos->view = FALSE;
 obj = 
 pointSenObj(_room->sen,p1,p2);
 
 //记录长按
 if(MS_DOWN==type)
 {
  long_obj = obj;
  long_time = getuptime();
 }
 
 //长按标识
 if(MS_MOVE==type)
 {
  if(obj!=long_obj)
  	long_obj = NULL;
  
  if(long_obj!=NULL)
  	if(getuptime()-long_time>100)
  {
   long_time = -1;
   game_sta->img = 1;
   //shake(10);
   sound_snd(snd_pop,0,0);
   touchCube(long_obj);
   long_obj = NULL;
   game_sta->img = 0;
  }
 }
 
 //长按消除
 if(MS_UP==type)
 {
  if(obj!=NULL&&long_time>0)
  {
   if(obj==game_sta)
   	 obj->img = !obj->img;
   else
    	touchCube(obj);
  }
  long_obj = NULL;
 }
 
 //移动鼠标
 if(MS_DOWN==type
   ||MS_MOVE==type)
 {
  if(obj!=NULL)
  {
   game_pos->view = TRUE;
   game_pos->x = obj->x;
   game_pos->y = obj->y;
  }
 }
 
 //退出页面
 if(KY_UP==type)
 {
  switch(p1)
  {
   case _BACK:
   //确保房间存在
   createMenu();
   gotoMove(&GM.rmHome,
    &GM.rmMenu,_LEFT);
   break;
  }
 }
}

//退出房间
void homeExit(void*rm)
{
 Room*room = rm;
 //释放数组
 free(bomb_arr);
 delSenAll(room->sen);
}

//跳转房间
int createHome()
{
 if(GM.rmHome==NULL)
 {
  GM.rmHome = 
  newRoom(SW,SH,100);
 }
 GM.rmHome->create = homeCreate;
 GM.rmHome->draw = homeDraw;
 GM.rmHome->exit = homeExit;
 GM.rmHome->event = homeEvent;
 return 0;
}






