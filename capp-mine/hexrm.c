#include "game.h"

//炸弹数，游戏时间，鼠标，插旗
extern Obj*bomb_num;//炸弹数
extern Obj*game_tim;//游戏时间
extern Obj*game_pos;
extern Obj*game_sta;//插旗模式

//返回主页、重载、下一关
extern Obj*game_home;
extern Obj*game_reload;
extern Obj*game_next;

//长按
extern int long_time;
extern Obj*long_obj;

//递归深度
extern int DEP;
extern int depth;
extern int one_bomb;

//存放炸弹位置
extern int*bomb_arr;
extern void drawCube(void*ob);
extern void drawNumSpr(void*ob);

extern void resetCube(ObjSen*link);
//extern int getCubeBomb(ObjSen*link);
extern int getHexBombNum(Obj*obj);
extern void touchHexCube(Obj*obj);
extern void runHexCube(Obj*obj);
//房间创建
void hexCreate(void*rm)
{
 Obj*obj;
 Spr*spr, *spr2;
 Room*room = rm;
 
 //页面文字
 GM.fnt_24_cn = (_FNT*)
 getRes("fnt/font_24_cn.fnt",
  tFnt)->id;
 
 GM.fnt_20_cn = (_FNT*)
 getRes("fnt/font_20_cn.fnt",
  tFnt)->id;
 
 GM.fnt_24 = (_FNT*)
 getRes("fnt/font_24.fnt",
  tFnt)->id;
 
 //炸弹数
 spr = (Spr*)
 getRes("spr/flag.spr",tSpr)->id;
 obj = newObj(20,20,spr);
 obj->img = 1;
 obj->draw = drawNumSpr;
 obj->data[0] = 0;
 obj->data[1] = 0;
 bomb_num = obj;
 roomAddObj(room,obj);
 
 //游戏时间
 spr = (Spr*)
 getRes("spr/timer.spr",tSpr)->id;
 obj = newObj(100,20,spr);
 obj->img = 1;
 obj->draw = drawNumSpr;
 obj->data[0] = 0;
 obj->data[1] = 0;
 obj->data[2] = getuptime();
 game_tim = obj;
 roomAddObj(room,obj);
 
 //横纵数目
 IMG_W = 32;
 MAP_N = SW/(IMG_W-IMG_W/4)-1;
 MAP_M = (SH-80)/IMG_W-1;
 
 //循环添加炸弹
 spr = (Spr*)
 getRes("spr/block2.spr",tSpr)->id;
 spr2 = (Spr*)
 getRes("spr/block2_mask.spr",tSpr)->id;
 for(int i = 0;i<MAP_N;i++)
 	for(int j = 0;j<MAP_M;j++)
 {
  obj = newObj((SW-MAP_W)/2+
  IMG_W*2-IMG_W/8+
  i*IMG_W-i*IMG_W/4,
  (SW-MAP_W)/2+IMG_W*2+60+
  IMG_W/2*(i%2)-j*2+
  j*IMG_W,spr);
  
  obj->data[0] = FALSE;
  obj->data[1] = 0;
  obj->group = 1;
  obj->draw = drawCube;
  obj->mask = spr2;
  //obj->mask_show = TRUE;
  roomAddObj(room,obj);
 }
 
 //插旗模式
 obj = newObj(SW-IMG_W,32,spr);
 obj->img = 0;
 game_sta = obj;
 roomAddObj(room,obj);
 
 //游戏鼠标
 spr = (Spr*)
 getRes("spr/focus2.spr",tSpr)->id;
 obj = newObj(SW/2,SH/2,spr);
 game_pos = obj;
 roomAddObj(room,obj);
 
 //返回主页、重载、下一关
 spr = (Spr*)
 getRes("spr/home.spr",tSpr)->id;
 obj = newObj(0,0,spr);
 obj->view = FALSE;
 game_home = obj;
 roomAddObj(room,obj);
 spr = (Spr*)
 getRes("spr/reload.spr",tSpr)->id;
 obj = newObj(0,0,spr);
 obj->view = FALSE;
 game_reload = obj;
 roomAddObj(room,obj);
 spr = (Spr*)
 getRes("spr/next.spr",tSpr)->id;
 obj = newObj(0,0,spr);
 obj->view = FALSE;
 game_next = obj;
 roomAddObj(room,obj);
 
 //关卡
 one_bomb = TRUE;//第一次点击未使用
 level = 0;
 is_run = FALSE;
 level_time = TIM_LEVEL;

 //存放炸弹位置
 bomb_arr = 
 malloc(sizeof(int)*MAP_N*MAP_M);
 game_sta->img = 0;
 resetCube(room->sen);
 bomb_num->data[1] = 
 getCubeBomb(GM.rmHex->sen);
}

//绘制房间
void hexDraw(void*rm)
{
 Room*room = rm;
 Cam*cam = &(room->cam);
 
 //第一局
 if(_room==room)
 if(cam->x==0)
 	if(is_run==FALSE)
 {
  is_run = TRUE;
  showNextDlg(&GM.rmHex,TRUE);
 }
 
 //时间计时
 if(_room==room)
 {
  game_tim->data[1] = (getuptime()-
   game_tim->data[2])/1000;
 }
 //暂停计时
 else
 {
  game_tim->data[2] = getuptime()
  -game_tim->data[2]+getuptime();
 }
 
 //绘制房间
 drawSenObjCam(room->sen,cam);
}


void hexEvent(int type,int p1,int p2)
{
 Obj*obj;
 game_pos->view = FALSE;
 obj = 
 pointSenObj(_room->sen,p1,p2);
 
 //记录长按
 if(MS_DOWN==type)
 {
  long_obj = obj;
  long_time = getuptime();
 }
 
 //长按标识
 if(MS_MOVE==type)
 {
  if(obj!=long_obj)
  	long_obj = NULL;
  
  if(long_obj!=NULL)
  	if(getuptime()-long_time>100)
  {
   long_time = -1;
   game_sta->img = 1;
   //shake(10);
   sound_snd(snd_pop,0,0);
   touchHexCube(long_obj);
   long_obj = NULL;
   game_sta->img = 0;
  }
 }
 
 //长按消除
 if(MS_UP==type)
 {
  if(obj!=NULL&&long_time>0)
  {
   if(obj==game_sta)
   	 obj->img = !obj->img;
   else
    	touchHexCube(obj);
  }
  long_obj = NULL;
 }
 
 //移动鼠标
 if(MS_DOWN==type
   ||MS_MOVE==type)
 {
  if(obj!=NULL)
  {
   game_pos->view = TRUE;
   game_pos->x = obj->x;
   game_pos->y = obj->y;
  }
 }
 
 //退出页面
 if(KY_UP==type)
 {
  switch(p1)
  {
   case _BACK:
   //确保房间存在
   createMenu();
   gotoMove(&GM.rmHex,
    &GM.rmMenu,_LEFT);
   break;
  }
 }
}




//退出房间
void hexExit(void*rm)
{
 Room*room = rm;
 //释放数组
 free(bomb_arr);
 delSenAll(room->sen);
}

//跳转房间
int createHex()
{
 if(GM.rmHex==NULL)
 {
  GM.rmHex = 
  newRoom(SW,SH,100);
 }
 GM.rmHex->create = hexCreate;
 GM.rmHex->draw = hexDraw;
 GM.rmHex->exit = hexExit;
 GM.rmHex->event = hexEvent;
 return 0;
}



